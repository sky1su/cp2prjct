#!/bin/bash
ver="1.02"
TMP_DIR="/tmp"
NO_ARGS=0
FILES_LIST=""
SHARE_NAME='/mnt/tmp_cifs'
usage ()
	{
	echo "Usage: `basename $0` -dir /path/to/dir [-type type] [-username username@domain] [-share //share_name/path/to/dir][-h]"
	echo -e "    \033[1mОпции:\033[0m"
	echo "-d /path/to/dir	Path to dir with selecting files"
	echo "-t [compactedvtk/cv,compactedgrdecl/cg,finalvtk/fv,finalgrdecl/fg,relief/r,cone/c, compactedroff/cr, finalroff/fr] list of types files for selecting"
        echo "-h This help."
	echo ""
	echo "(c) 2018 GeoGrid, ver. $ver"
	}
if [ $# -eq "$NO_ARGS" ]
	then
		usage
		exit $E_OPTEROR
fi
while getopts ":d:t:u:s:h" Option
	do
		case $Option in
		d ) DIR_NAME=$OPTARG;;
		t ) TYPE=$OPTARG;;
		u ) SHARE_USERNAME=$OPTARG;;
		s ) SHARE_NAME=$OPTARG;;
		* | h ) echo "Invalid option."
			usage
			exit $E_OPTERROR;;
		esac
	done
TYPE_LIST=(${TYPE//,/ })

for ITEM in ${TYPE_LIST[@]}
	do
	case $ITEM in
	compactedvtk )
			compactedvtk_list=`ls $DIR_NAME/out_c | awk '/compacted.*vtk$/'`
			for file in $compactedvtk_list
				do
				FILES_LIST+="$DIR_NAME/out_c/$file "
				done
			;;
	cv )
                        compactedvtk_list=`ls $DIR_NAME/out_c | awk '/compacted.*vtk$/'`
                        for file in $compactedvtk_list
                                do
                                        FILES_LIST+="$DIR_NAME/out_c/$file "
                                done
                        ;;
	compactedroff )
			compactedroff_list=`ls $DIR_NAME/out_c | awk '/compacted.*vtk$/'`
			;;
	cr )
			compactedroff_list=`ls $DIR_NAME/out_c | awk '/compacted.*vtk$/'`
			;;

	compactedgrdecl )
			compactedgrdecl_list=`ls $DIR_NAME/out_c | awk '/compacted.*vtk.ecl$/'`
			for file in $compactedgrdecl_list
	                        do
                                        FILES_LIST+="$DIR_NAME/out_c/$file "
                                done
			;;
	cg )
                        compactedgrdecl_list=`ls $DIR_NAME/out_c | awk '/compacted.*vtk.ecl$/'`
                        for file in $compactedgrdecl_list
                                do
                                        FILES_LIST+="$DIR_NAME/out_c/$file "
                                done
                        ;;

	finalvtk )
			finalvtk_list=`ls $DIR_NAME/out_c | awk '/final.*vtk$/'`
			for file in $finalvtk_list
                                do
                                        FILES_LIST+="$DIR_NAME/out_c/$file "
                                done
			;;
        fv )
                        finalvtk_list=`ls $DIR_NAME/out_c | awk '/final.*vtk$/'`
                        for file in $finalvtk_list
                                do
                                        FILES_LIST+="$DIR_NAME/out_c/$file "
                                done
                        ;;
	finalgrdecl )
			finalgrdecl_list=`ls $DIR_NAME/out_c | awk '/final.*vtk.ecl$/'`
			for file in $finalgrdecl_list
                                do
                                        FILES_LIST+="$DIR_NAME/out_c/$file "
                                done
			;;
	fg )
                        finalgrdecl_list=`ls $DIR_NAME/out_c | awk '/final.*vtk.ecl$/'`
                        for file in $finalgrdecl_list
                                do
                                        FILES_LIST+="$DIR_NAME/out_c/$file "
                                done
                        ;;
	relief )
			relief_list=`ls $DIR_NAME/output | awk '/relief.grd$/'`
			for file in $relief_list
                                do
                                        FILES_LIST+="$DIR_NAME/output/$file "
                                done
			;;
 	 r )
		 	relief_list=`ls $DIR_NAME/output | awk '/relief.grd$/'`
                        for file in $relief_list
                                do
                                        FILES_LIST+="$DIR_NAME/output/$file "
                                done
                        ;;
  	cone )        	
			cone_list=`ls $DIR_NAME/out_c | awk '/compacteddh_conus_total.grd$/'`
                        for file in $cone_list
                                do
                                        FILES_LIST+="$DIR_NAME/out_c/$file "
                                done
                        ;;
	c )
			cone_list=`ls $DIR_NAME/out_c | awk '/compacteddh_conus_total.grd$/'`
                        for file in $cone_list
                                do
                                        FILES_LIST+="$DIR_NAME/out_c/$file "
                                done
                        ;;
	* )
			echo "Invalid type - " $ITEM
			usage
			exit $E_OPTERROR
			;;
	esac
	done
ARCHIVE="$TMP_DIR/MazayCalc_$(date +%Y%m%d-%H.%M).7z"
TMP_RESULT=`/usr/bin/7za a $ARCHIVE $FILES_LIST | grep -o 'Everything is Ok'`
if [ $# -eq "Everything is Ok" ]
        then
                echo "7zip result: $TMP_RESULT"
	else	echo "Something goes wrong!"
fi
